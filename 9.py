import sys
import math
import itertools
import collections

inp = sys.stdin.read().strip()

in_garbage = False
ignore_next = False
cur_level = 0

sum_levels = 0
num_garbage = 0

for c in inp:
    if not in_garbage:
        if c == '<':
            in_garbage = True
        elif c == '{':
            cur_level += 1
            sum_levels += cur_level
        elif c == '}':
            cur_level -= 1
    else:
        if ignore_next: 
            ignore_next = False
            continue
        elif c == '!':
            ignore_next = True
        elif c == '>':
            in_garbage = False
        else:
            num_garbage += 1

print sum_levels
print num_garbage

