import sys
import collections
import math

steps = sys.stdin.readline().strip().split(',')
cnt = collections.defaultdict(int)

max_dist = 0

def dist():
    h = abs(cnt['ne'] + cnt['se'] - (cnt['sw'] + cnt['nw']))
    v = abs(cnt['n'] + 0.5*cnt['ne'] + 0.5*cnt['nw'] - (cnt['s'] + 0.5*cnt['se'] + 0.5*cnt['sw']))
    if h-2*v>0:
        return h
    else:
        return 2*v

for step in steps:
    cnt[step] += 1
    max_dist = max(max_dist, dist())

print dist()
print max_dist