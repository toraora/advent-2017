import sys
import collections
import math

ps = dict()
n = 0

for l in sys.stdin.readlines():
    l = l.strip()
    if not l:
        continue
    p, v, a = l.split(', ')
    p = [int(i) for i in p[3:len(p)-1].split(',')]
    v = [int(i) for i in v[3:len(v)-1].split(',')]
    a = [int(i) for i in a[3:len(a)-1].split(',')]
    ps[n] = [p,v,a,n]
    n += 1

for _ in range(1000):
    this_iter_visited = dict()
    this_iter_to_remove = set()
    for pn, part in ps.iteritems():
        pos, vel, acc, part_n = part
        vel[0],vel[1],vel[2] = vel[0]+acc[0],vel[1]+acc[1],vel[2]+acc[2]
        pos[0],pos[1],pos[2] = pos[0]+vel[0],pos[1]+vel[1],pos[2]+vel[2]
        key = str(pos)
        if key in this_iter_visited:
            this_iter_to_remove.add(pn)
            this_iter_to_remove.add(this_iter_visited[key])
        else:
            this_iter_visited[key] = pn

    for tr in this_iter_to_remove:
        del ps[tr]

print len(ps)

exit()

min_dist = None
min_dist_part = None
for part in ps:
    pos = part[0]
    dist = pos[0]**2 + pos[1]**2 + pos[2]**2
    if min_dist == None or dist < min_dist:
        min_dist_part = part
        min_dist = dist

print min_dist_part[3]


