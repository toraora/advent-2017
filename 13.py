import sys
import collections
import math

inp = sys.stdin.readlines()
wall = dict()
depth = 0

for l in inp:
    parts = l.strip().split(': ')
    n = int(parts[0])
    r = int(parts[1])
    wall[n] = r
    depth = n

def score_w_delay(delay):
    score = 0

    for i in range(depth + 1):
        if i in wall:
            if (i + delay) % (2 * wall[i] - 2) == 0:
                return False

    return True

d = 0

print score_w_delay(0)
while 1:
    if score_w_delay(d):
        print d; exit()
    d += 1