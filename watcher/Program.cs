﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace watcher
{
    class WatcherConfig
    {
        public List<string> argNames;
        public string watchPattern;
        public List<CallbackSpec> callbacks;
    }

    class CallbackSpec
    {
        public string name;
        public string execute;
        public string args;
        [JsonConverter(typeof(StringEnumConverter))]
        public ConsoleColor color;
    }

    class Program
    {
        static WatcherConfig config;

        static void Main(string[] args)
        {
            var configFile = new FileInfo("watcher.config.json");
            if (!configFile.Exists)
                throw new FileNotFoundException("No config file found!");
            config = JsonConvert.DeserializeObject<WatcherConfig>(
                File.ReadAllText(configFile.FullName)
            );

            var argIdx = 0;
            foreach (var argName in config.argNames)
            {
                config.watchPattern = config.watchPattern.Replace(argName, args[argIdx]);
                foreach (var callback in config.callbacks)
                {
                    callback.execute = callback.execute.Replace(argName, args[argIdx]);
                    callback.args = callback.args.Replace(argName, args[argIdx]);
                }
                argIdx ++;
            }
            
            var fsw = new FileSystemWatcher(".", config.watchPattern);
            fsw.Changed += OnChangeCallback;
            fsw.Created += OnChangeCallback;
            fsw.EnableRaisingEvents = true;

            Console.WriteLine($"Started watcher with pattern ./{config.watchPattern}");

            for (;;)
                Thread.Sleep(1000);
        }

        static object _processLock = new object();
        static List<Process> processes = new List<Process>();
        static void OnChangeCallback(object e, FileSystemEventArgs args)
        {
            lock (_processLock)
            {
                foreach (var process in processes)
                {
                    try {
                        if (!process.HasExited)
                            process.Kill();
                    }
                    catch { }
                }

                processes.Clear();

                var width = Console.WindowWidth;
                var displayStr = " Change detected; reloading... ";
                var numPadding = (int) ((width - displayStr.Length)/2);

                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine($"{new String('=', numPadding)}{displayStr}{new String('=', numPadding)}");

                foreach (var callback in config.callbacks)
                {
                    var info = new ProcessStartInfo();
                    info.FileName = callback.execute;
                    info.Arguments = callback.args;
                    info.UseShellExecute = false;
                    info.RedirectStandardOutput = true;
                    info.RedirectStandardError = true;
                    
                    var process = new Process();
                    process.StartInfo = info;
                    process.OutputDataReceived += (ev, a) => ThreadSafeConsoleWrite(callback.name, a.Data, callback.color);
                    process.ErrorDataReceived += (ev, a) => ThreadSafeConsoleWrite(callback.name, a.Data, ConsoleColor.Red);
                    process.Start();
                    process.BeginErrorReadLine();
                    process.BeginOutputReadLine();

                    processes.Add(process);
                }
            }
        }

        static object _stdoutLock = new object();
        static void ThreadSafeConsoleWrite(string header, string data, ConsoleColor color)
        {
            if (string.IsNullOrEmpty(data))
                return;

            lock (_stdoutLock)
            {
                Console.ForegroundColor = color;
                Console.Write($"[{header}]");
                Console.ResetColor();
                Console.WriteLine($": {data}");
            }
        }
    }
}
