import sys
from collections import defaultdict

inp = sys.stdin.readlines()

conns = defaultdict(list)
csd = []

for idx, l in enumerate(inp):
    parts = l.strip().split('/')
    csd += parts
    conns[parts[0]] += [(idx, parts[1])]
    conns[parts[1]] += [(idx, parts[0])]

m_len = 0
m = 0
def r(cur, used, score):
    global m, m_len
    done = True
    for idx, c in conns[cur]:
        if idx in used:
            continue
        r(c, used | set([idx]), score + 2 * int(c))
        done = False
    if done:
        f_len = len(used)
        if f_len > m_len:
            m_len = f_len
            m = 0
        if f_len == m_len:
            f_score = score - int(cur)
            m = max(m, f_score)

r('0', set(), 0)

print(m)
    


