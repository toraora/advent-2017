t = 0
for line in open('4.in', 'r').readlines():
    words = [str(sorted(word)) for word in line.split()]
    if len(words) == len(set(words)):
        t += 1
print(t)