import sys

inp = sys.stdin.readline().strip()


arr = list(range(256))

def rev(start, end):
    while start < end:
        arr[start % len(arr)], arr[end % len(arr)] = arr[end % len(arr)], arr[start % len(arr)]
        start += 1
        end -= 1

"""
nums = [int(i.strip()) for i in inp.split(',')]

cur = 0
skip = 0
for num in nums:
    rev(cur, cur + num - 1)
    cur += num + skip
    skip += 1

print arr[0] * arr[1]
"""

## part 2

nums = [ord(c) for c in inp] + [17, 31, 73, 47, 23]

arr = list(range(256))

cur = 0
skip = 0
for _ in range(64):
    for num in nums:
        rev(cur, cur + num - 1)
        cur += num + skip
        skip += 1

s = ''
for i in range(16):
    xored = reduce(lambda a, b: a ^ b, arr[16*i:16*(i+1)])
    t = hex(xored)[2:]
    if len(t) == 1:
        s += '0'
    s += t

print s
