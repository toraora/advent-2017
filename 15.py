import sys
import collections
import math

gen_a_fac = 16807
gen_b_fac = 48271

div = 2147483647
chk = 2**16 - 1

a = int(sys.stdin.readline())
b = int(sys.stdin.readline())

ok = 0

for _ in range(5000000):
    while 1:
        a = (a * gen_a_fac) % div
        if a % 4 == 0:
            break
    while 1:
        b = (b * gen_b_fac) % div
        if b % 8 == 0:
            break
    if (a - b) & chk == 0:
        ok += 1


print ok