import math
import sys
import collections
from itertools import chain

grid = collections.defaultdict(lambda: '.')

inp = sys.stdin.readlines()
h = len(inp) // 2

d = [(-1,0),(0,1),(1,0),(0,-1)]
cur_d = 0

for i, l in enumerate(inp):
    l = l.strip()
    for j, c in enumerate(l):
        grid[(i-h,j-h)] = c

n = 0
cur = [0,0]
for _ in range(10000000):
    i, j = cur
    if grid[(i,j)] == '.':
        grid[(i,j)] = 'W'
        cur_d -= 1
    elif grid[(i,j)] == 'W':
        grid[(i,j)] = '#'
        n += 1
    elif grid[(i,j)] == '#':
        grid[(i,j)] = 'F'
        cur_d += 1
    else:
        grid[(i,j)] = '.'
        cur_d += 2

    cur[0] += d[cur_d % 4][0]
    cur[1] += d[cur_d % 4][1]

print n
