import sys
import math
import itertools

start = 0
parents = dict()
children_d = dict()
wts = dict()

inp = sys.stdin.readlines()

for tower in inp:
    parts = tower.split(' (')
    name = parts[0]
    weight = weight = int(parts[1].split(')')[0])
    wts[name] = weight
    
    children_d[name] = []
    children = 0
    if '->' in tower:
        children = tower.strip().split('-> ')[1].split(', ')
        for child in children:
            parents[child] = name
            children_d[name] += [child]
            
    start = name

cur = start
while 1:
    if cur in parents:
        cur = parents[cur]
    else:
        break

root = cur

t_wts = dict()
problem_node = 0
diff = 0

def c(node):
    global diff, problem_node

    children = children_d[node]
    if not children:
        t_wts[node] = wts[node]
        return wts[node]
    c_wts = [c(child) for child in children]

    if len(children) >= 3 and len(set(c_wts)) != 1 and problem_node == 0:
        problem_node = node
        common = 0
        uniq = 0
        for v in set(c_wts):
            if c_wts.count(v) == 1:
                uniq = v
            else:
                common = v
        diff = common - uniq
    
    this_wt = wts[node] + sum(c_wts)
    t_wts[node] = this_wt
    return this_wt
c(root)

print problem_node, diff

    

