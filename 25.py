import sys
from collections import defaultdict

tape = defaultdict(int)
cursor = 0
states = dict()

cur_state = sys.stdin.readline().strip().split(' ')[-1][0]
num_steps = int(sys.stdin.readline().strip().split(' ')[5])

while not sys.stdin.closed:
    instr = dict()
    sys.stdin.readline()
    t_state = ''
    try:
        t_state = sys.stdin.readline().strip()[-2]
    except:
        break
    sys.stdin.readline()
    t_write = int(sys.stdin.readline().strip()[-2])
    t_dir = 1 if sys.stdin.readline().strip().split(' ')[-1] == 'right.' else -1
    t_nextstate = sys.stdin.readline().strip()[-2]
    instr[0] = (t_write, t_dir, t_nextstate)
    sys.stdin.readline()
    t_write = int(sys.stdin.readline().strip()[-2])
    t_dir = 1 if sys.stdin.readline().strip().split(' ')[-1] == 'right.' else -1
    t_nextstate = sys.stdin.readline().strip()[-2]
    instr[1] = (t_write, t_dir, t_nextstate)
    states[t_state] = instr

for _ in range(num_steps):
    write, d, nextstate = states[cur_state][tape[cursor]]
    tape[cursor] = write
    cursor += d
    cur_state = nextstate

print(sum(tape.values()))

    