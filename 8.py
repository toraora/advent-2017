import sys
import math
import itertools
import collections

regs = collections.defaultdict(lambda: 0)

max_val = 0

for line in sys.stdin.readlines():
    line = line.strip()
    parts = line.split(' ')
    reg = parts[0]
    op = parts[1]
    val = int(parts[2])
    reg_c = parts[4]
    op_c = parts[5]
    val_c = int(parts[6])

    if eval('regs["%s"] %s %i' %(reg_c, op_c, val_c)):
        if op == 'inc':
            regs[reg] += val
        else:
            regs[reg] -= val

    if regs[reg] > max_val:
        max_val = regs[reg]
    
print max(regs.values())
print max_val
