import sys
import collections
import math

# inp = sys.stdin.read().strip().split(',')

inp = open('16.in', 'r').read().strip().split(',')

pro = [c for c in 'abcdefghijklmnop']
#pro = [c for c in 'abcde']

seen = set()
arr = []
n = 0
while 1:
    seen.add(''.join(pro))
    arr += [(n, ''.join(pro))]
    for step in inp:
        if step[0] == 's':
            i = -int(step[1:])
            pro = pro[i:] + pro[:i]
        elif step[0] == 'x':
            x,y = [int(i) for i in step[1:].split('/')]
            pro[x],pro[y] = pro[y],pro[x]
        elif step[0] == 'p':
            x,y = step[1:].split('/')
            a = pro.index(x)
            b = pro.index(y)
            pro[a],pro[b] = pro[b],pro[a]
    if ''.join(pro) in seen:
        break
    n += 1


print ''.join(pro)