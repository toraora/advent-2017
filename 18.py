import sys
import collections
import Queue

instrs = sys.stdin.readlines()

def isnumeric(s):
    try:
        int(s)
    except ValueError:
        return False
    else:
        return True

B_sent = 0
regs_A = collections.defaultdict(int)
regs_B = collections.defaultdict(int)
que_A = Queue.Queue()
que_B = Queue.Queue()
regs_A['p'] = 0
regs_B['p'] = 1
# returns (
#   locked: boolean
#   send: integer
#   next: integer
# )
def step(cur, regs, que):
    global B_sent
    send = None

    l = instrs[cur]
    l = l.strip()
    parts = l.split(' ')
    inst = parts[0]
    if inst == 'set':
        if not isnumeric(parts[2]):
            regs[parts[1]] = regs[parts[2]]
        else:
            regs[parts[1]] = int(parts[2])    
    elif inst == 'add':
        if not isnumeric(parts[2]):
            regs[parts[1]] += regs[parts[2]]
        else:
            regs[parts[1]] += int(parts[2])
    elif inst == 'mul':
        if not isnumeric(parts[2]):
            regs[parts[1]] *= regs[parts[2]]
        else:
            regs[parts[1]] *= int(parts[2])
    elif inst == 'mod':
        if not isnumeric(parts[2]):
            regs[parts[1]] %= regs[parts[2]]
        else:
            regs[parts[1]] %= int(parts[2])
    elif inst == 'jgz':
        check = 0
        if not isnumeric(parts[1]):
            check = regs[parts[1]]
        else:
            check = int(parts[1])
        if check > 0:
            jmp = 0
            if not isnumeric(parts[2]):
                jmp = regs[parts[2]]
            else:
                jmp = int(parts[2])
            cur += jmp - 1
    elif inst == 'snd':
        if regs is regs_B:
            B_sent += 1
        if not isnumeric(parts[1]):
            send = regs[parts[1]]
        else:
            send = int(parts[1])
    elif inst == 'rcv':
        if not que.empty():
            regs[parts[1]] = que.get()
        else:
            return (True, None, cur)
    return (False, send, cur + 1)

cur_A = 0
cur_B = 0
As = Bs = None
while 1:
    Al, As, cur_A = step(cur_A, regs_A, que_A)
    if As:
        que_B.put(As)
    Bl, Bs, cur_B = step(cur_B, regs_B, que_B)
    if Bs:
        que_A.put(Bs)
    if Al and Bl:
        print B_sent; exit()

    