import sys
import math
import itertools

#inp = int(input())
inp = [int(i) for i in sys.stdin.read().split()]
inp_cnt = len(inp)
#inp = [[int(i) for i in l.split()] for l in sys.stdin.readlines()]

n = 0
seen = set()
seen_arr = []
while 1:
    seen.add(str(inp))
    seen_arr += [str(inp)]
    n += 1
    max_idx = inp.index(max(inp))
    cnt = inp[max_idx]
    inp[max_idx] = 0
    for _ in range(cnt):
        inp[(max_idx + _ + 1) % inp_cnt] += 1
    if str(inp) in seen:
        break

print n
print n - seen_arr.index(str(inp))