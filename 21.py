import math
import sys
import collections
from itertools import chain

grid = """.#.
..#
###""".split('\n')

inp = sys.stdin.readlines()

rules = dict()
for l in inp:
    l = l.strip()
    i, o = l.split(' => ')
    i_grid = i.split('/')
    o_grid = o.split('/')

    for _ in range(4):
        rules[''.join(chain(*i_grid))] = o_grid
        i_grid = zip(*i_grid[::-1])
    
    i_grid = [r[::-1] for r in i_grid]
    for _ in range(4):
        rules[''.join(chain(*i_grid))] = o_grid
        i_grid = zip(*i_grid[::-1])

for _ in range(18):
    new = None
    if len(grid) % 2 == 0:
        new = [[0 for q in range(len(grid)*3/2)] for qq in range(len(grid)*3/2)]
        for i in range(0, len(grid), 2):
            for j in range(0, len(grid), 2):
                new_i = i * 3 / 2
                new_j = j * 3 / 2
                cur = [g[j:j+2] for g in grid[i:i+2]]
                cur_s = ''.join(chain(*cur))
                replace = rules[cur_s]
                for di, r in enumerate(replace):
                    for dj, c in enumerate(r):
                        new[new_i+di][new_j+dj] = c

    else:
        new = [[0 for q in range(len(grid)*4/3)] for qq in range(len(grid)*4/3)]
        for i in range(0, len(grid), 3):
            for j in range(0, len(grid), 3):
                new_i = i * 4 / 3
                new_j = j * 4 / 3
                cur = [g[j:j+3] for g in grid[i:i+3]]
                cur_s = ''.join(chain(*cur))
                replace = rules[cur_s]
                for di, r in enumerate(replace):
                    for dj, c in enumerate(r):
                        new[new_i+di][new_j+dj] = c
 

    grid = new

print ''.join(chain(*grid)).count('#')
    
