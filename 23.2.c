#include <stdio.h>
#include <math.h>

int main() {
    long h = 0;

    for (long b = 108400; b <= 125400; b += 17) {
        // composite check on b
        for (long d = 2; d <= sqrt(b) + 1; d++) {
            if (b % d == 0) { 
                h = h + 1;
                break;
            }
        }        
    }

    printf("%li\n", h);
    return 0;
}