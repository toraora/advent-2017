import sys
import collections
import math

inp = sys.stdin.readlines()

graph = collections.defaultdict(lambda: set())
num = 0

for line in inp:
    num += 1
    line = line.strip()
    first, rest = line.split(' <-> ')
    for node in rest.split(', '):
        node = node.strip()
        graph[int(first)].add(int(node))


cnt = 0
visited = set()
while len(visited) != num:
    cnt += 1
    lst = [list(set(graph.keys()) - visited)[0]]
    while len(lst):
        cur = lst.pop()
        visited.add(cur)
        for child in graph[cur]:
            if child in visited:
                continue
            lst.append(child)

print len(set(visited))
print cnt
