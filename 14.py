import sys
import collections
import math

inp = sys.stdin.readline().strip()



def knot_hash(inp):

    arr = list(range(256))

    def rev(start, end):
        while start < end:
            arr[start % len(arr)], arr[end % len(arr)] = arr[end % len(arr)], arr[start % len(arr)]
            start += 1
            end -= 1

    """
    nums = [int(i.strip()) for i in inp.split(',')]

    cur = 0
    skip = 0
    for num in nums:
        rev(cur, cur + num - 1)
        cur += num + skip
        skip += 1

    print arr[0] * arr[1]
    """

    ## part 2

    nums = [ord(c) for c in inp] + [17, 31, 73, 47, 23]

    arr = list(range(256))

    cur = 0
    skip = 0
    for _ in range(64):
        for num in nums:
            rev(cur, cur + num - 1)
            cur += num + skip
            skip += 1

    s = ''
    cnt = 0
    b = ''
    for i in range(16):
        xored = reduce(lambda a, b: a ^ b, arr[16*i:16*(i+1)])
        t = hex(xored)[2:]
        cnt += bin(xored).count('1')
        temp = bin(xored)[2:]
        b += '0' * (8 - len(temp)) + temp
        if len(t) == 1:
            s += '0'
        s += t

    return b, cnt


grid = []
tot = 0
for _ in range(128):
    s = inp + '-%i' %_
    a, b = knot_hash(s)
    tot += b
    grid += [[int(i) for i in a]]

print tot

dirs = ((-1,0),(1,0),(0,-1),(0,1))
visited = set()
def d(i, j):
    lst = [(i,j)]
    while len(lst):
        i,j = lst.pop()
        visited.add((i,j))
        for dx, dy in dirs:
            ni = i + dx
            nj = j + dy
            if ni >= 0 and ni < 128 and nj >= 0 and nj < 128 and (ni,nj) not in visited and grid[ni][nj] == 1:
                lst += [(ni,nj)]

n = 0
for i in range(128):
    for j in range(128):
        if (i,j) not in visited and grid[i][j] == 1:
            d(i,j)
            n += 1

print n