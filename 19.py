import sys
import collections
import math

maze = sys.stdin.readlines()

cur = [0, maze[0].index('|')]
d = (1, 0)
visited = ''

n = 0
while 1:
    n += 1
    cur = [cur[0] + d[0], cur[1] + d[1]]
    if cur[0] < 0 or cur[0] >= len(maze) or cur[1] < 0 or cur[1] >= len(maze[0]):
        break
    l = maze[cur[0]][cur[1]]
    if l == ' ':
        break
    elif l == '+':
        if d == (1,0) or d == (-1,0): # need to turn horizontal
            if cur[1] != 0 and maze[cur[0]][cur[1]-1] != ' ' and maze[cur[0]][cur[1]-1] != '|':
                d = (0, -1)
            else:
                d = (0, 1)
        else:
            if cur[0] != 0 and maze[cur[0]-1][cur[1]] != ' ' and maze[cur[0]-1][cur[1]] != '-':
                d = (-1, 0)
            else:
                d = (1, 0)
    elif l.isalpha():
        visited += l

print visited
print n