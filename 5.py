import sys
import math
import itertools

inp = [int(i) for i in sys.stdin.read().split()]

n = 0
idx = 0
while 1:
    n += 1
    cur = inp[idx]
    if cur >= 3:
        inp[idx] -= 1
    else:
        inp[idx] += 1
    idx += cur
    if idx < 0 or idx >= len(inp):
        break

print(n)